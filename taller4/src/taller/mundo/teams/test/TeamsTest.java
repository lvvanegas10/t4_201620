package taller.mundo.teams.test;

import junit.framework.TestCase;
import static org.junit.Assert.*;
import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
import taller.mundo.teams.BubbleSortTeam;
import taller.mundo.teams.HibridoTeam;
import taller.mundo.teams.InsertionSortTeam;
import taller.mundo.teams.MergeSortTeam;
import taller.mundo.teams.QuickSortTeam;
import taller.mundo.teams.SelectionSortTeam;

public class TeamsTest extends TestCase{

	/**
	 * Prueba InsertionSort
	 * Resultados: para cada tipo de ordenamiento Ascendente o Descendente se prueba que el
	 * arreglo est� ordenada para cada uno de los siguientes casos
	 * 1. Si el arreglo inicial ya esta ordenado.
	 * 2. Si el arreglo inicial est� en orden inverso.
	 * 3. Si el arreglo est� totalmente desordenado.
	 */
	public void testInsertionSort()
	{
		InsertionSortTeam o = new InsertionSortTeam();

		//PROBAR CON ORDEN ASCENDENTE
		Comparable[] compruebaA ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] ascendente ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] inversoA ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] randomA ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(ascendente, TipoOrdenamiento.ASCENDENTE);			
		assertArrayEquals(compruebaA, ascendente);
		o.sort(inversoA, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(compruebaA, inversoA);
		o.sort(randomA, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(compruebaA, randomA);

		//PROBAR CON ORDEN DESCENDENTE
		Comparable[] compruebaD ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] descendente ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] inversoD = {0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] randomD ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(descendente, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, descendente);
		o.sort(inversoD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, inversoD);
		o.sort(randomD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, randomD);

	}
	/**
	 * Prueba SelectionSort
	 * Resultados: para cada tipo de ordenamiento Ascendente o Descendente se prueba que el
	 * arreglo est� ordenada para cada uno de los siguientes casos
	 * 1. Si el arreglo inicial ya esta ordenado.
	 * 2. Si el arreglo inicial est� en orden inverso.
	 * 3. Si el arreglo est� totalmente desordenado.
	 */
	public void testSelectionSort()
	{
		SelectionSortTeam o = new SelectionSortTeam();

		//PROBAR CON ORDEN ASCENDENTE
		Comparable[] compruebaA ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] ascendente ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] inversoA ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] randomA ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(ascendente, TipoOrdenamiento.ASCENDENTE);			
		assertArrayEquals(compruebaA, ascendente);
		o.sort(inversoA, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(compruebaA, inversoA);
		o.sort(randomA, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(compruebaA, randomA);

		//PROBAR CON ORDEN DESCENDENTE
		Comparable[] compruebaD ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] descendente ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] inversoD = {0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] randomD ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(descendente, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, descendente);
		o.sort(inversoD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, inversoD);
		o.sort(randomD, TipoOrdenamiento.DESCENDENTE);

		assertArrayEquals(compruebaD, randomD);

	}
	/**
	 * Prueba BubbleSort
	 * Resultados: para cada tipo de ordenamiento Ascendente o Descendente se prueba que el
	 * arreglo est� ordenada para cada uno de los siguientes casos
	 * 1. Si el arreglo inicial ya esta ordenado.
	 * 2. Si el arreglo inicial est� en orden inverso.
	 * 3. Si el arreglo est� totalmente desordenado.
	 */
	public void testBubbleSort()
	{
		BubbleSortTeam o = new BubbleSortTeam();

		//PROBAR CON ORDEN ASCENDENTE
		Comparable[] compruebaA ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] ascendente ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] inversoA ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] randomA ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(ascendente, TipoOrdenamiento.ASCENDENTE);			
		assertArrayEquals(compruebaA, ascendente);
		o.sort(inversoA, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(compruebaA, inversoA);
		o.sort(randomA, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(compruebaA, randomA);

		//PROBAR CON ORDEN DESCENDENTE
		Comparable[] compruebaD ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] descendente ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] inversoD = {0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] randomD ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(descendente, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, descendente);
		o.sort(inversoD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, inversoD);
		o.sort(randomD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, randomD);

	}
	/**
	 * Prueba Hibrido
	 * Resultados: para cada tipo de ordenamiento Ascendente o Descendente se prueba que el
	 * arreglo est� ordenada para cada uno de los siguientes casos
	 * 1. Si el arreglo inicial ya esta ordenado.
	 * 2. Si el arreglo inicial est� en orden inverso.
	 * 3. Si el arreglo est� totalmente desordenado.
	 */
	public void testHibridoSort()
	{
		HibridoTeam o = new HibridoTeam();

		//PROBAR CON ORDEN ASCENDENTE
		Comparable[] compruebaA ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] ascendente ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] inversoA ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] randomA ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(ascendente, TipoOrdenamiento.ASCENDENTE);			
		assertArrayEquals(compruebaA, ascendente);
		o.sort(inversoA, TipoOrdenamiento.ASCENDENTE);	
		assertArrayEquals(compruebaA, inversoA);
		o.sort(randomA, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(compruebaA, randomA);

		//PROBAR CON ORDEN DESCENDENTE
		Comparable[] compruebaD ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] descendente ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] inversoD = {0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] randomD ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(descendente, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, descendente);
		o.sort(inversoD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, inversoD);
		o.sort(randomD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, randomD);
	}
	/**
	 * Prueba MergeSort
	 * Resultados: para cada tipo de ordenamiento Ascendente o Descendente se prueba que el
	 * arreglo est� ordenada para cada uno de los siguientes casos
	 * 1. Si el arreglo inicial ya esta ordenado.
	 * 2. Si el arreglo inicial est� en orden inverso.
	 * 3. Si el arreglo est� totalmente desordenado.
	 */
	public void testMergeSort()
	{
		MergeSortTeam o = new MergeSortTeam();

		//PROBAR CON ORDEN ASCENDENTE
		Comparable[] compruebaA ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] ascendente ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] inversoA ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] randomA ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(ascendente, TipoOrdenamiento.ASCENDENTE);			
		assertArrayEquals(compruebaA, ascendente);
		o.sort(inversoA, TipoOrdenamiento.ASCENDENTE);	
		assertArrayEquals(compruebaA, inversoA);
		o.sort(randomA, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(compruebaA, randomA);

		//PROBAR CON ORDEN DESCENDENTE
		Comparable[] compruebaD ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] descendente ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] inversoD = {0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] randomD ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(descendente, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, descendente);
		o.sort(inversoD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, inversoD);
		o.sort(randomD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, randomD);

	}
	/**
	 * Prueba QuickSort
	 * Resultados: para cada tipo de ordenamiento Ascendente o Descendente se prueba que el
	 * arreglo est� ordenada para cada uno de los siguientes casos
	 * 1. Si el arreglo inicial ya esta ordenado.
	 * 2. Si el arreglo inicial est� en orden inverso.
	 * 3. Si el arreglo est� totalmente desordenado.
	 */
	public void testQuickSort()
	{
		QuickSortTeam o = new QuickSortTeam();

		//PROBAR CON ORDEN ASCENDENTE
		Comparable[] comprueba ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] ascendent ={0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] inverso ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] random ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(ascendent, TipoOrdenamiento.ASCENDENTE);			
		assertArrayEquals(comprueba, ascendent);
		o.sort(inverso, TipoOrdenamiento.ASCENDENTE);	
		assertArrayEquals(comprueba, inverso);
		o.sort(random, TipoOrdenamiento.ASCENDENTE);
		assertArrayEquals(comprueba, random);

		//PROBAR CON ORDEN DESCENDENTE
		Comparable[] compruebaD ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] descendente ={10,9,8,7,6,5,4,3,2,1,0};
		Comparable[] inversoD = {0,1,2,3,4,5,6,7,8,9,10};
		Comparable[] randomD ={1,0,4,10,3,5,8,2,6,9,7};

		o.sort(descendente, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, descendente);
		o.sort(inversoD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, inversoD);
		o.sort(randomD, TipoOrdenamiento.DESCENDENTE);
		assertArrayEquals(compruebaD, randomD);
	}
}
