package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;


public class MergeSortTeam extends AlgorithmTeam
{

	public MergeSortTeam()
	{
		super("Merge sort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		return merge_sort(lista, orden);
	}


	private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
	{
		// Trabajo en Clase
		int hi = lista.length -1;
		int lo = 0;
		if(hi<=lo) return lista;

		int mid = lo +(hi-lo)/2;

		Comparable[] derecha = new Comparable[hi-mid];
		Comparable[] izquierda = new Comparable[mid+1];

		for (int i = 0; i <= mid; i++) izquierda[i] = lista[i];
		for (int i = mid +1 ; i <= hi; i++) derecha[i-mid-1] = lista[i];


		Comparable[] a = merge_sort(izquierda, orden);
		Comparable[] b = merge_sort(derecha, orden);
		Comparable[] c= merge(a, b, orden);	
		
		for (int i = 0; i < c.length && c.length== lista.length; i++) {
			lista[i]= c[i];
		}
		return lista;
	}

	private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
	{
		// Trabajo en Clase 
		int i =0;
		int j=0;
		int tamano= izquierda.length + derecha.length;    	  	
		Comparable[] aux = new Comparable[tamano];


		for (int m = 0; m < aux.length; m++) {			
			if(i >=  izquierda.length) aux[m]= derecha[j++];			
			else if(j >=  derecha.length) 
			{
				aux[m]= izquierda[i];
				i++;
			}
			else if( derecha[j].compareTo(izquierda[i]) > 0){
				if(orden== TipoOrdenamiento.DESCENDENTE) aux[m]= derecha[j++]; 				
				else aux[m]= izquierda[i++];				
			}
			else {
				if(orden== TipoOrdenamiento.ASCENDENTE) aux[m]= derecha[j++];
				else aux[m]= izquierda[i++];				
			}				
		}
		return aux;
	}  
}
