package taller.mundo.teams;

/*
 * QuickSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyderecha (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * CODIGO APOYADO EN http://homepage.cs.uiowa.edu/~sriram/21/spring06/code/LaforeQuickSort/quickSort2.java
 */

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
public class QuickSortTeam extends AlgorithmTeam
{
	public QuickSortTeam()
	{
		super("Quicksort (*)");
		userDefined = true;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		quicksort(list, 0, list.length-1,orden);
		if(orden== TipoOrdenamiento.DESCENDENTE)
		{
			return reverseArray(list);
		}
		return list;
	}

	private static void quicksort(Comparable[] list, int izquierda, int derecha, TipoOrdenamiento orden)
	{
		int size = derecha-izquierda+1;
		if(size <= 3)  			
			manual(list,izquierda, derecha);			 
		else                           
		{
			int median = media3(list, izquierda, derecha);
			int partition = particionCom(list, izquierda, derecha, median);
			quicksort(list,izquierda, partition-1,orden);
			quicksort(list,partition+1, derecha,orden);
		}
	}

	private static int particionCom(Comparable[] list, int izquierda, int derecha,int pivot)
	{

		int izquierdaPtr = izquierda;             
		int derechaPtr = derecha - 1;       

		while(true)
		{
			while( list[++izquierdaPtr].compareTo(list[pivot])<0 )  
				;                                  
			while( list[--derechaPtr].compareTo(list[pivot])>0 )
				;                                 
			if(izquierdaPtr >= derechaPtr)     
				break;                    
			else                         
				cambio(list,izquierdaPtr, derechaPtr);  
		}  
		cambio(list,izquierdaPtr, derecha-1);        
		return izquierdaPtr;                 
	}  	


	private static void cambio(Comparable[] lista, int i, int j)
	{
		Comparable temp= lista[i];
		lista[i]= lista[j];
		lista[j]= temp;
	}
	private static int media3( Comparable[] list,int izquierda, int derecha) {
		int center = (izquierda+derecha)/2;
		if( list[izquierda].compareTo( list[center])>0 )
			cambio(list,izquierda, center);		
		if( list[izquierda].compareTo(list[derecha])>0 )
			cambio(list,izquierda, derecha);		
		if( list[center].compareTo( list[derecha] )>0)
			cambio(list,center, derecha);
		cambio(list,center, derecha-1);             
		return derecha-1; 
	}
	public Comparable[] reverseArray(Comparable[] list)
	{
		for(int i = 0; i < list.length/2; i++)
		{
			Comparable temp = list[i];
			list[i] = list[list.length - 1 - i];
			list[list.length - 1 - i] = temp; 
		}
		return list;
	}
	public static void manual(Comparable[] list, int izquierda, int derecha)
	{
		int size = derecha-izquierda+1;
		if(size <= 1)
			return;         
		if(size == 2)
		{               
			if( list[izquierda].compareTo(list[derecha])>0 )
				cambio(list,izquierda, derecha);
			return;
		}
		else              
		{             
			if( list[izquierda].compareTo( list[derecha-1])>0 )
				cambio(list,izquierda, derecha-1);                
			if( list[izquierda].compareTo( list[derecha])>0 )
				cambio(list,izquierda, derecha);                  
			if( list[derecha-1].compareTo( list[derecha] )>0)
				cambio(list,derecha-1, derecha);               
		}
	}
}