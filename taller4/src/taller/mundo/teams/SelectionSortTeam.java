package taller.mundo.teams;

/*
 * SelectionSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class SelectionSortTeam extends AlgorithmTeam
{
	public SelectionSortTeam()
	{
		super("Selection Sort (-)");
		userDefined = false;
	}

	@Override
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden)
	{
		return selectionSort(list, orden);
	}

	/**
      Ordena un arreglo de enteros, usando Ordenamiento por selección.
      @param arr Arreglo de enteros.
	 **/
	private Comparable[] selectionSort(Comparable[] arr, TipoOrdenamiento orden)
	{
		// Para hacer en casa
		int N = arr.length;
		for (int i = 0; i < N; i++) {
			int min=i;
			for (int j = i+1; j < N; j++) {
				if(orden.equals(TipoOrdenamiento.ASCENDENTE) && arr[j].compareTo(arr[min])<0)
					min=j;
				else if(orden.equals(TipoOrdenamiento.DESCENDENTE) && arr[j].compareTo(arr[min])>0)
					min=j;			
			}
			Comparable t =arr[i];
			arr[i]=arr[min];
			arr[min]=t;
		}
		return arr;
	}
}
