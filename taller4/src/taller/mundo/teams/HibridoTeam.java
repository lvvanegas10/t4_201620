package taller.mundo.teams;

import taller.mundo.AlgorithmTournament.TipoOrdenamiento;
/**
 * CODIGO APOYADO EN http://homepage.cs.uiowa.edu/~sriram/21/spring06/code/LaforeQuickSort/quickSort2.java
 */

public class HibridoTeam extends AlgorithmTeam{

	 public HibridoTeam() {	
          super("Hibrido Sort (*)");
          userDefined = false;
     }
	
	public Comparable[] sort(Comparable[] list, TipoOrdenamiento orden) {
		
		hibridoSort(list,0, list.length-1);
		if(orden== TipoOrdenamiento.DESCENDENTE)
		{
			return reverseArray(list);
		}
		return list;
	}
	public Comparable[] reverseArray(Comparable[] list)
	{
		for(int i = 0; i < list.length/2; i++)
		{
			Comparable temp = list[i];
			list[i] = list[list.length - 1 - i];
			list[list.length - 1 - i] = temp; 
		}
		return list;
	}
	public void hibridoSort(Comparable[] list, int start, int end)
    {
        if (start < end)
        {
          
            if ((end - start) < 9)
            {
                this.InsertionSort(list, start, end + 1);
            }
            else
            {
                int part = this.partition(list, start, end);
                this.hibridoSort(list, start, part - 1);
                this.hibridoSort(list, part + 1, end);
            }
        }
    }
    public void InsertionSort(Comparable[] list, int start, int end)
    {
        for (int x = start + 1; x < end; x++)
        {
            Comparable val = list[x];
            int j = x - 1;
            while (j >= 0 && val.compareTo(list[j])<0)
            {
                list[j + 1] = list[j];
                j--;
            }
            list[j + 1] = val;
        }
    }
    private int partition(Comparable[] list, int leftIndex, int rightIndex)
    {
        int left = leftIndex;
        int right = rightIndex;
        Comparable pivot = list[leftIndex];
        while (left < right)
        {
            if (list[left].compareTo(pivot)<0)
            {
                left++;
                continue;
            }
            if (list[right] .compareTo(pivot)>0)
            {
                right--;
                continue;
            }
            Comparable tmp = list[left];
            list[left] = list[right];
            list[right] = tmp;
            left++;
        }
        return left;
    }

}
